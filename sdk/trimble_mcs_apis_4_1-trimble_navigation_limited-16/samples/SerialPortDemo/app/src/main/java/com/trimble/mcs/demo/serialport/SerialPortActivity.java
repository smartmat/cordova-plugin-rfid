package com.trimble.mcs.demo.serialport;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Button;

import com.trimble.mcs.serialport.SerialPort;

/**
 * Connect Serial cable adapter and null modem cable to computer.
 * Use a serial terminal program to send text to this app.
 */

public class SerialPortActivity extends Activity {
    private final static String LOG_TAG = "SerialPortDemo";
    private final static int BAUD_RATE = 9600;
    private final static int BUFFER_SIZE = 512;

    private byte[] send_text;
    private byte[] read_text;

    private SerialPort my_serial_port;
    private File[] my_ports;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serial_port);

        my_ports = SerialPort.getSerialPortList();
        read_text = new byte[BUFFER_SIZE];

        try{
            send_text = "Hello, world!\r\n".getBytes("US-ASCII");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    // try to open serial port and show its info
    public void openPort(View view) {
        if (my_ports.length == 0) {
            Toast.makeText(getApplicationContext(), "No serial port found", Toast.LENGTH_SHORT)
            .show();
        } else {
            try{
                my_serial_port = new SerialPort(my_ports[0], BAUD_RATE, 0);
            }
            catch(IOException e) {
                Log.e(LOG_TAG, "IO Error while trying to communicate with the Serial Port.\n", e);
            }
            serialInfo();
            ((Button)findViewById(R.id.btn_open)).setEnabled(false);
        }
    }

    // try to close the open serial port
    public void closePort(View view) {
        if (my_serial_port != null && my_serial_port.isOpen()) {
            try {
                my_serial_port.close();
            } catch (IOException e) {
                Log.e(LOG_TAG, "IO Error while trying to communicate with the Serial Port.\n", e);
            }
            ((Button)findViewById(R.id.btn_open)).setEnabled(true);
        }
        else {
            Toast.makeText(getApplicationContext(), "Serial port not Opened", Toast.LENGTH_SHORT)
            .show();
        }
    }

    // show serial port info
    private void serialInfo() {
        TextView textbox = (TextView)findViewById(R.id.outputText);
        try{
            textbox.append("Baud Rate: " + Integer.toString(my_serial_port.getBaudrate()) + "\n");
            textbox.append("Char Size: " + Integer.toString(my_serial_port.getCharSize()) + "\n");
            textbox.append("Stop Bit:" + Integer.toString(my_serial_port.getStopBits()) + "\n");
            textbox.append("CTRL Bits:" + Integer.toString(my_serial_port.getControlSignal()) + "\n");
            textbox.append("Flow CTRL:" + Integer.toString(my_serial_port.getFlowControl()) + "\n");
            textbox.append("Parity bits:" + Integer.toString(my_serial_port.getParity()) + "\n");
        }
        catch(IOException e) {
          Log.e(LOG_TAG, "IO Error while trying to communicate with the Serial Port.\n", e);
        }
    }

    // send bytes over serial
    public void send(View view) {
        if (my_serial_port != null && my_serial_port.isOpen()) {
            try {
                my_serial_port.write(send_text);

                Toast.makeText(getApplicationContext(), "Sent data", Toast.LENGTH_SHORT)
                .show();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Exception happened", Toast.LENGTH_SHORT)
                .show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Serial port not Opened", Toast.LENGTH_SHORT)
            .show();
        }
    }

    // read data from serial port
    public void read(View view) {
        if (my_serial_port != null && my_serial_port.isOpen()) {
            try {
                my_serial_port.read(read_text);
                String s = new String(read_text);
                TextView textbox = (TextView)findViewById(R.id.outputText);
                textbox.append(s);
                ((ScrollView)findViewById(R.id.scrollView1)).fullScroll(View.FOCUS_DOWN);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Exception happened", Toast.LENGTH_SHORT)
                .show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Serial port not Opened", Toast.LENGTH_SHORT)
            .show();
        }
    }
}
