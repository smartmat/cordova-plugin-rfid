package com.trimble.mcs.demo.systeminfo;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.trimble.mcs.systeminfo.SystemInfo;

public class SystemInfoActivity extends Activity {
    private LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system_info);
        layout = (LinearLayout)findViewById(R.id.LinearLayout1);

        featureList(SystemInfo.getBluetoothAvailable(), "Bluetooth");
        featureList(SystemInfo.getCameraAvailable(), "Camera");
        featureList(SystemInfo.getRFIDAvailable(), "RFID");
        featureList(SystemInfo.getScannerAvailable(), "Scanner");
        featureList(SystemInfo.getWifiAvailable(), "WiFi");
        featureList(SystemInfo.getWWANAvailable(), "WWAN");
        addSeparator();

        featureList(1, Integer.toString(SystemInfo.getCPUSpeed()) + " MHz CPU");
        featureList(1, Integer.toString(SystemInfo.getRAMSize()) + " MB RAM");
        featureList(1, Integer.toString(SystemInfo.getInternalStorageSize()) + " MB Internal Storage");
        featureList(1, Integer.toString(SystemInfo.getSharedStorageSize()) + " MB Shared Storage");
        addSeparator();

        featureList("USB Storage State: " + SystemInfo.getUsbStorageState());
        featureList("USB Storage Path: " + SystemInfo.getUsbStorageDirectory().getPath());
        featureList("SD Card State: " + SystemInfo.getRemovableSDCardStorageState());
        featureList("SD Card Path: " + SystemInfo.getRemovableSDCardStorageDirectory().getPath());
        addSeparator();

        featureList("Model Number: " + Integer.toString(SystemInfo.getDeviceModel()));
        featureList("OS Build Number: " + Integer.toString(SystemInfo.getOSBuild()));
        featureList("OS Revison: " + SystemInfo.getOSRevision());
        featureList("Serial Number: " + SystemInfo.getSerialNumber());
    }

    // Adds a TextView to layout with pertinent information
    private void featureList(int has_fature, String the_feature) {
        TextView tv = new TextView(this);
        String does_have;

        if (has_fature != 0) {
            does_have = "This device has ";
        } else {
            does_have = "This device does NOT have ";
        }

        tv.setText(does_have + the_feature);
        layout.addView(tv);
    }

    private void featureList(String the_feature) {
        TextView tv = new TextView(this);
        tv.setText(the_feature);
        layout.addView(tv);
    }

    // draw a line to separate items in the list
    private void addSeparator() {
        View separator = new View(this);
        separator.setLayoutParams( new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 3));
        separator.setBackgroundColor(Color.DKGRAY);
        layout.addView(separator);
    }
}
