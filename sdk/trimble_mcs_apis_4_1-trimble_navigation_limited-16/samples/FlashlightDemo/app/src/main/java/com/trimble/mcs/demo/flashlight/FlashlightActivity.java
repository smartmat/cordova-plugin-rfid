package com.trimble.mcs.demo.flashlight;

import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.trimble.mcs.flashlight.Flashlight;

public class FlashlightActivity extends Activity {
	private static final String LOG_TAG = "FlashlightDemo";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_flashlight);
	}

	public void toggleLight(View view) {
		try{
			// light is off, turn it on.
			if (Flashlight.getMode() == Flashlight.MODE_OFF) {
				((Button)findViewById(R.id.toggleButton)).setText(R.string.light_on);
				Flashlight.setMode(Flashlight.MODE_ON);
			}
			// light is on, turn it off.
			else {
				((Button)findViewById(R.id.toggleButton)).setText(R.string.light_off);
				Flashlight.setMode(Flashlight.MODE_OFF);
			}
		}
		catch(IOException e) {
			Log.e(LOG_TAG, "IO Error while trying to toggle light.\n", e);
		}
	}
}
