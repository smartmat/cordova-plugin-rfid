package com.trimble.mcs.demo.gps;

import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.trimble.mcs.gps.AssistedGps;

public class AGPSActivity extends Activity {
    private static final String LOG_TAG = "AGPSDemo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agps);
        checkAGPS();
    }

    @Override
    // If returning from disabling AGPS in Settings show the changes.
    protected void onResume() {
        super.onResume();
        checkAGPS();
    }

    // Read and display the AGPS states.
    private void checkAGPS() {
        boolean status;
        try{
            status = AssistedGps.getAgpsEnabled(AssistedGps.AGPS_ONLINE);
            ((TextView)findViewById(R.id.agps_online)).setText(Boolean.toString(status));

            status = AssistedGps.getAgpsEnabled(AssistedGps.AGPS_OFFLINE);
            ((TextView)findViewById(R.id.agps_offline)).setText(Boolean.toString(status));

            status = AssistedGps.getAgpsEnabled(AssistedGps.AGPS_AUTONOMOUS);
            ((TextView)findViewById(R.id.agps_auto)).setText(Boolean.toString(status));

            status = AssistedGps.getGPSAAEnabled();
            ((TextView)findViewById(R.id.agps_gpsaa)).setText(Boolean.toString(status));

            status = AssistedGps.isGPSAASupported();
            ((TextView)findViewById(R.id.agps_gpsaa_support)).setText(Boolean.toString(status));
        }
        catch(IOException e) {
            Log.e(LOG_TAG, "IO Error while trying to communicate with the GPS module.\n", e);
        }
    }

    public void toggleOffline(View view) {
        boolean status;
        try{
            status = AssistedGps.getAgpsEnabled(AssistedGps.AGPS_OFFLINE);
            AssistedGps.setAgpsEnabled(AssistedGps.AGPS_OFFLINE, !status);
        }
        catch(IOException e) {
            Log.e(LOG_TAG, "IO Error while trying to communicate with the GPS module.\n", e);
        }
        checkAGPS();
    }

    // GPS module must have GPS Accuracy Algorithm support for this to change state.
    public void toggleGPSAA(View view) {
        boolean status;
        try{
            status = AssistedGps.getGPSAAEnabled();
            if (status) {
                AssistedGps.setGPSAAEnabled(false);
            }
            else {
                AssistedGps.setGPSAAEnabled(true);
            }
        }
        catch(IOException e) {
            Log.e(LOG_TAG, "IO Error while trying to communicate with the GPS module.\n", e);
        }
        checkAGPS();
    }
}
