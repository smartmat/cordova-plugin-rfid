package com.trimble.mcs.demo.scanner;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.trimble.mcs.barcode.v1.BarcodeConstants;
import com.trimble.mcs.barcode.v1.BarcodeManager;
import com.trimble.mcs.barcode.v1.BarcodeStatusCallback;
import com.trimble.mcs.barcode.v1.BarcodeTypes;

public class ScannerActivity extends Activity {
	private final static String LOG_TAG = "ScannerDemo";

	BroadcastReceiver mRecvr;
	IntentFilter mFilter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scanner);

		mRecvr = new BroadcastReceiver() {
			public void onReceive(Context context, Intent intent) {
				onScanComplete(context, intent);
			}
		};

		mFilter = new IntentFilter();
		mFilter.addAction(BarcodeConstants.ACTION_SCAN_STOPPED);
		mFilter.addAction(BarcodeConstants.ACTION_BARCODE_SCANNED);

		BarcodeStatusCallback cb = new BarcodeStatusCallback() {
			@Override
			public void onAPIReady() {
				// Called when BarcodeManager API is fully initialized
			    // Perform initial scanner configuration here.
				scannerApiReady();
			}
		};

		BarcodeManager.initForGlobalSettings(this, cb);
	}

	private void scannerApiReady() {
		// Enable custom beep sound
		Uri uri = Uri.parse("android.resource://com.trimble.mcs.demo.scanner/raw/lock");
		BarcodeManager.SuccessBeep.setUri(uri);
		BarcodeManager.SuccessBeep.setEnabled(true);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		BarcodeManager.deinit();
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(mRecvr, mFilter);
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(mRecvr);
	}

	public void startScan(View view) {
		BarcodeManager.enable();
		int ret = BarcodeManager.startScanWithTimeout(5000);
		if (ret != BarcodeConstants.SUCCESS)
			Log.d(LOG_TAG, "Error starting scan: " + ret);
	}

	private void onScanComplete(Context context, Intent intent) {
		String act = intent.getAction();
		if (act.equals(BarcodeConstants.ACTION_BARCODE_SCANNED)) {
			String data = intent.getStringExtra("data");
			int type = intent.getIntExtra("symbology", BarcodeTypes.NOTIFIER_SYMBOLOGY_UNKNOWN);
			String typeStr = BarcodeTypes.getTypeString(type);

			((EditText) findViewById(R.id.etData)).setText(data);
			((EditText) findViewById(R.id.etType)).setText(typeStr);
		} else if (act.equals(BarcodeConstants.ACTION_SCAN_STOPPED)) {
			Log.d(LOG_TAG, "Scanning stopped");
		}
	}
}
