package com.trimble.mcs.demo.rfid;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.crashlytics.android.Crashlytics;
import com.trimble.mcs.rfid.v1.RfidConstants;
import com.trimble.mcs.rfid.v1.RfidException;
import com.trimble.mcs.rfid.v1.RfidManager;
import com.trimble.mcs.rfid.v1.RfidParameters;
import com.trimble.mcs.rfid.v1.RfidStatusCallback;
import io.fabric.sdk.android.Fabric;

public class RfidActivity extends Activity {
	private final static String LOG_TAG = "RfidDemo";

	private BroadcastReceiver mRecvr;
	private IntentFilter mFilter;
	private boolean mScanning = false;
	private Button mBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Fabric.with(this, new Crashlytics());
		setContentView(R.layout.activity_rfid);

		mBtn = (Button)findViewById(R.id.btn_scan);

		mRecvr = new BroadcastReceiver() {
			public void onReceive(Context context, Intent intent) {
				onScanComplete(context, intent);
			}
		};

		mFilter = new IntentFilter();
		mFilter.addAction(RfidConstants.ACTION_RFID_TAG_SCANNED);
		mFilter.addAction(RfidConstants.ACTION_RFID_STOP_SCAN_NOTIFICATION);

		RfidStatusCallback cb = new RfidStatusCallback() {
			@Override
			public void onAPIReady() {
				// Called when RfidManager API is fully initialized.
			    // Perform initial RFID configuration here.
				onRfidReady();
			}
		};

		try {
			RfidManager.init(this, RfidConstants.SESSION_SCOPE_PRIVATE, cb);
		} catch (RfidException e) {
			Log.e(LOG_TAG, "Error initializing RFID Manager.", e);
		}
	}

	private void onRfidReady() {
		try {
			// Set output mode to 'Intent' mode so that broadcast
			// intents will be fired tags are scanned
			RfidParameters parms = RfidManager.getParameters();
			parms.setOutputMode(RfidConstants.OUTPUT_MODE_INTENT);
			RfidManager.setParameters(parms);
		} catch (RfidException e) {
			Log.e(LOG_TAG, "Error setting RFID parameters.", e);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			RfidManager.deinit();
		} catch (RfidException e) {
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(mRecvr, mFilter);
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(mRecvr);
		if (mScanning) {
			try {
				RfidManager.stopScan();
			} catch (RfidException e) {
			}
			mScanning = false;
			mBtn.setText(R.string.btn_scan);
		}
	}

	public void startScan(View view) {
		try {
			if (!mScanning) {
				RfidManager.startScan();
				mScanning = true;
				mBtn.setText(R.string.btn_stop_scan);
			}
			else {
				RfidManager.stopScan();
				mScanning = false;
				mBtn.setText(R.string.btn_scan);
			}
		} catch (RfidException e) {
			Log.e(LOG_TAG, "Error attempting to start/stop scan.", e);
		}
	}

	private void onScanComplete(Context context, Intent intent) {
		String act = intent.getAction();

		if (act.equals(RfidConstants.ACTION_RFID_TAG_SCANNED)) {
			String tagId = intent.getStringExtra(RfidConstants.RFID_FIELD_ID);
			((EditText)findViewById(R.id.etData)).setText(tagId);
		} else if (act.equals(RfidConstants.ACTION_RFID_STOP_SCAN_NOTIFICATION)) {
			Log.d(LOG_TAG, "Scanning stopped");
		}
	}
}
