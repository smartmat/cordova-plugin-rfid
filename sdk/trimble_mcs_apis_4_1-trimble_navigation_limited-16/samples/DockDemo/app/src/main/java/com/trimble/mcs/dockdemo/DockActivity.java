package com.trimble.mcs.dockdemo;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.trimble.mcs.dock.DockConstants;
import com.trimble.mcs.dock.DockException;
import com.trimble.mcs.dock.DockManager;
import com.trimble.mcs.dock.DockStatusCallback;

public class DockActivity extends Activity {
    public static final String TAG = "DockDemo";

    private TextView mStatusText;
    private ToggleButton mLedButton;
    private ToggleButton mWakeButton;
    private DockEventReceiver mMessageReceiver = null;
    private IntentFilter mDockActionFilter;

    private DockStatusCallback mStatusCallback = new DockStatusCallback() {
        @Override
        public void onAPIReady() {
            Log.d(TAG, "DockStatusCallback.onAPIReady() called");

            // Set the initial dock status
            if (!updateAllDockInfo())
                mStatusText.setText("Dock API init failed!");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mStatusText = (TextView) findViewById(R.id.dockStatus);
        mLedButton = (ToggleButton)findViewById(R.id.ledButton);
        mWakeButton = (ToggleButton)findViewById(R.id.wakeButton);

        mDockActionFilter = new IntentFilter(DockConstants.ACTION_DOCK_ATTACHED);
        mDockActionFilter.addAction(DockConstants.ACTION_DOCK_DETACHED);
        mDockActionFilter.addAction(DockConstants.ACTION_PIN_CHANGE);
        mMessageReceiver = new DockEventReceiver();
        initDockApi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mMessageReceiver, mDockActionFilter);
        updateAllDockInfo();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mMessageReceiver);
    }

    @Override
    protected void onDestroy() {
        try {
            DockManager.deinit();
        } catch (DockException e) {
            Log.e(TAG, "DockManager.deinit failed", e);
        }
        super.onDestroy();
    }

    public void initDockApi() {
        mStatusText.setText("Initializing Dock API...");

        // Initialize the DockManager
        try {
            DockManager.init(getApplicationContext(), mStatusCallback);
        } catch (DockException e) {
            mStatusText.setText("Dock API init failed!");
        }
      }

    private boolean updateAllDockInfo() {
        try {
            updateDockStatus(DockManager.isDocked());
            updatePinStatus();
            updateLedState();
            updateWakeState();
            return true;
        } catch (DockException e) {
            mStatusText.setText(e.toString());
            return false;
        }
    }

    private void updateDockStatus(boolean attached)
    {
        try {
            int dockType = DockManager.getDockType();
            if (attached)
                mStatusText.setText("Attached: " + DockManager.dockTypeDescription(dockType));
            else
                mStatusText.setText("Detached");
        } catch (DockException e) {
            mStatusText.setText(e.toString());
            return;
        }
    }

    private void updateLedState() {
        try {
            boolean state = DockManager.getLedState();
            mLedButton.setChecked(state);
        } catch (DockException e) {
            mStatusText.setText(e.toString());
        }
    }

    private void updateWakeState() {
        try {
            boolean state = DockManager.getWakeOnDockEvent();
            mWakeButton.setChecked(state);
        } catch (DockException e) {
            mStatusText.setText(e.toString());
        }
    }

    private void updatePinStatus() {
        try {
            byte pins = DockManager.getInputPinStates();
            for (int i=0; i<8; i++) {
                String viewName = "bit" + i;
                int id = getResources().getIdentifier(viewName, "id", this.getPackageName());
                ToggleButton b = (ToggleButton)findViewById(id);
                b.setChecked(((pins >> i) & 1) == 1);
            }
        } catch (DockException e) {
            mStatusText.setText(e.toString());
        }
    }

    public void onLedToggleClicked(View v) {
        try {
            boolean on = ((ToggleButton)v).isChecked();
            DockManager.setLedState(on);
            updatePinStatus();
        } catch (DockException e) {
            mStatusText.setText(e.toString());
        }
    }

    public void onWakeToggleClicked(View v) {
        try {
            boolean on = ((ToggleButton)v).isChecked();
            DockManager.setWakeOnDockEvent(on);
        } catch (DockException e) {
            mStatusText.setText(e.toString());
        }
    }

    private class DockEventReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();

            Log.d(TAG, "MainActivity received new broadcast intent: " + action);

            if (action.equals(DockConstants.ACTION_DOCK_ATTACHED)) {
                updateAllDockInfo();
            } else if (action.equals(DockConstants.ACTION_DOCK_DETACHED)) {
                updateDockStatus(false);
                updateWakeState();
            } else if (action.equals(DockConstants.ACTION_PIN_CHANGE)) {
                updatePinStatus();
                updateLedState();
            }
        }
    }
}
