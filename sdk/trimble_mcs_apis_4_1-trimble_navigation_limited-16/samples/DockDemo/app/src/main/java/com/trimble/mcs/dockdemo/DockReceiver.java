package com.trimble.mcs.dockdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.trimble.mcs.dock.DockConstants;

/*
 * This class illustrates how to start an activity when the
 * ACTION_DOCK_ATTACHED broadcast intent is received. Note that this
 * receiver is registered in AndroidManifest.xml.
 */
public class DockReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        // Launch the app when a new DOCK_ATTACHED event is received
        if (intent.getAction().equals(DockConstants.ACTION_DOCK_ATTACHED)) {
            Intent newIntent = new Intent(context, DockActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                               Intent.FLAG_ACTIVITY_CLEAR_TOP |
                               Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.getApplicationContext().startActivity(newIntent);
        }
    }
}
