package org.apache.cordova.rfid;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

public class Rfid extends CordovaPlugin {

  private final static String LOG_TAG = "RFID Driver";


  // RFID actions
  // ------------
    private void getTags(
    String msg, 
    CallbackContext callbackContext
  ) {
    if (msg == null || msg.length() == 0) {
      callbackContext.error("Empty message!");
    } else {
      callbackContext.success(msg); 
    }
  }

  // cordova com
  // -----------
  @Override
  public boolean execute(
    String action, 
    JSONArray args, 
    CallbackContext callbackContext
  ) throws JSONException {
    if ("getTags".equals(action)) {
      getTags(args.getString(0), callbackContext);
      return true;
    }

    
    return false;
  }

}