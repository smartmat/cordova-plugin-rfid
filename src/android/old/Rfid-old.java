// package org.apache.cordova.rfid;

// import org.apache.cordova.CordovaPlugin;
// import org.apache.cordova.CallbackContext;
// import org.json.JSONArray;
// import org.json.JSONObject;
// import org.json.JSONException;

// import android.content.BroadcastReceiver;
// import android.content.Context;
// import android.content.Intent;
// import android.content.IntentFilter;
// import android.os.Bundle;
// import android.util.Log;

// import com.trimble.mcs.rfid.v1.RfidConstants;
// import com.trimble.mcs.rfid.v1.RfidException;
// import com.trimble.mcs.rfid.v1.RfidManager;
// import com.trimble.mcs.rfid.v1.RfidParameters;
// import com.trimble.mcs.rfid.v1.RfidStatusCallback;

// public class Rfid extends CordovaPlugin {

//     // private void startScan(CallbackContext callbackContext) {
//   //   try {
//   //     if (!mScanning) {
//   //       RfidManager.startScan();
//   //       mScanning = true;
//   //       callbackContext.success(); 
//   //     }
//   //     else {
//   //       callbackContext.error("already scanning");
//   //     }
//   //   } catch (RfidException e) {
//   //     Log.e(LOG_TAG, "Error attempting to start scan.", e);
//   //     // callbackContext.error(e);
//   //   }
//   // }

//       // if ("startScan".equals(action)) {
//     //   startScan(callbackContext);
//     //   return true;
//     // }

//     // if ("stopScan".equals(action)) {
//     //   stopScan(callbackContext);
//     //   return true;
//     // }

//   // private void stopScan(CallbackContext callbackContext) {
//   //   try {
//   //     if (mScanning) {
//   //       RfidManager.stopScan();
//   //       mScanning = false;
//   //       callbackContext.success();
//   //     }
//   //     else {
//   //       callbackContext.error("not scanning");
//   //     }
//   //   } catch (RfidException e) {
//   //     Log.e(LOG_TAG, "Error attempting to stop scan.", e);
//   //     // callbackContext.error(e);
//   //   }
//   // }

//   // private void getTags(
//   //   String msg, 
//   //   CallbackContext callbackContext
//   // ) {
//   //   if (msg == null || msg.length() == 0) {
//   //     callbackContext.error("Empty message!");
//   //   } else {
//   //     callbackContext.success(msg); 
//   //   }
//   // }

//   private final static String LOG_TAG = "RfidDemo";

//   private BroadcastReceiver mRecvr;
//   private IntentFilter mFilter;
//   private boolean mScanning = false;

//   // driver functions
//   // ----------------
//   private void getTags(
//     String msg, 
//     CallbackContext callbackContext
//   ) {
//     if (msg == null || msg.length() == 0) {
//       callbackContext.error("Empty message!");
//     } else {
//       callbackContext.success(msg); 
//     }
//   }

//   // cordova com
//   // -----------
//   @Override
//   public boolean execute(
//     String action, 
//     JSONArray args, 
//     CallbackContext callbackContext
//   ) throws JSONException {
//     if ("getTags".equals(action)) {
//       getTags(args.getString(0), callbackContext);
//       return true;
//     }

//     // if ("startScan".equals(action)) {
//     //   startScan(callbackContext);
//     //   return true;
//     // }

//     // if ("stopScan".equals(action)) {
//     //   stopScan(callbackContext);
//     //   return true;
//     // }
    
//     return false;
//   }

//   // android events
//   // --------------
//   @Override
//   protected void onCreate(Bundle savedInstanceState) {

//     // save instance if case it gets destroyed
//     super.onCreate(savedInstanceState);

//     // where event data is sent on trigger
//     mRecvr = new BroadcastReceiver() {
//       public void onReceive(Context context, Intent intent) {
//         onEventReceived(context, intent);
//       }
//     };

//     // which events to watch
//     mFilter = new IntentFilter();   
//     mFilter.addAction(RfidConstants.ACTION_RFID_TAG_SCANNED);
//     mFilter.addAction(RfidConstants.ACTION_RFID_START_SCAN_NOTIFICATION);
//     mFilter.addAction(RfidConstants.ACTION_RFID_STOP_SCAN_NOTIFICATION);

//     // event: rfid reader api ready
//     RfidStatusCallback cb = new RfidStatusCallback() {
//       @Override
//       public void onAPIReady() {
//         // Called when RfidManager API is fully initialized.
//         // Perform initial RFID configuration here.
//         onRfidAPIReady();
//       }
//     };

//     // initialize the rfid reader
//     try {
//       RfidManager.init(this, RfidConstants.SESSION_SCOPE_PRIVATE, cb);
//     } catch (RfidException e) {
//       Log.e(LOG_TAG, "Error initializing RFID Manager.", e);
//     }
//   }

//   @Override
//   protected void onDestroy() {
//     super.onDestroy();
//     try {
//       RfidManager.deinit();
//     } catch (RfidException e) {
//     }
//   }

//   @Override
//   protected void onResume() {
//     super.onResume();
//     registerReceiver(mRecvr, mFilter);
//   }

//   @Override
//   protected void onPause() {
//     super.onPause();
//     unregisterReceiver(mRecvr);
//     if (mScanning) {
//       try {
//         RfidManager.stopScan();
//       } catch (RfidException e) {
//       }
//       mScanning = false;
//     }
//   }

//   // RFID actions
//   // ------------
//   public void startScan(CallbackContext callbackContext) {
//     try {
//       if (!mScanning) {
//         RfidManager.startScan();
//         mScanning = true;
//         callbackContext.success(); 
//       }
//       else {
//         callbackContext.error("already scanning");
//       }
//     } catch (RfidException e) {
//       Log.e(LOG_TAG, "Error attempting to start scan.", e);
//       callbackContext.error(e);
//     }
//   }

//   public void stopScan(CallbackContext callbackContext) {
//     try {
//       if (mScanning) {
//         RfidManager.stopScan();
//         mScanning = false;
//         callbackContext.success();
//       }
//       else {
//         callbackContext.error("not scanning");
//       }
//     } catch (RfidException e) {
//       Log.e(LOG_TAG, "Error attempting to stop scan.", e);
//       callbackContext.error(e);
//     }
//   }

//   // RFID events
//   // -----------
//   private void onRfidAPIReady() {
//     try {
//       // Set output mode to 'Intent' mode so that broadcast
//       // ACTION_RFID_TAG_SCANNED will be fired when tags are scanned
//       RfidParameters parms = RfidManager.getParameters();
//       parms.setOutputMode(RfidConstants.OUTPUT_MODE_INTENT);
//       RfidManager.setParameters(parms);
//     } catch (RfidException e) {
//       Log.e(LOG_TAG, "Error setting RFID parameters.", e);
//     }
//   }

//   private void onEventReceived(Context context, Intent intent) {
//     String act = intent.getAction();

//     // tag scanned
//     if (act.equals(RfidConstants.ACTION_RFID_TAG_SCANNED)) {
//       // TODO: update javascript object with tags
//       String tagId = intent.getStringExtra(RfidConstants.RFID_FIELD_ID);
//     } 
//     // scanning stopped
//     else if (act.equals(RfidConstants.ACTION_RFID_START_SCAN_NOTIFICATION)) {
//       // TODO: update javascript object with status
//       Log.d(LOG_TAG, "Scanning stopped");
//     }
//     // scanning stopped
//     else if (act.equals(RfidConstants.ACTION_RFID_STOP_SCAN_NOTIFICATION)) {
//       // TODO: update javascript object with status
//       Log.d(LOG_TAG, "Scanning stopped");
//     }
//   }
// }