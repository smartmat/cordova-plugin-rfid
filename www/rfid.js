var exec = require('cordova/exec');

exports.getTags = function(arg0, success, error) {
	console.log('plugin rfid.js: getTags');
  exec(success, error, "Rfid", "getTags", [arg0]);
};

// exports.initRfid = function(success, error) {
// 	console.log('plugin rfid.js: initRfid');
//   exec(success, error, "Rfid", "initRfid", []);
// };

// exports.startScan = function(success, error) {
// 	console.log('plugin rfid.js: startScan');
//   exec(success, error, "Rfid", "startScan", []);
// };

// exports.stopScan = function(success, error) {
// 	console.log('plugin rfid.js: startScan');
//   exec(success, error, "Rfid", "startScan", []);
// };
